let guessTheNumber = (el) => {
    const baseUrl = 'https://www.drukzo.nl.joao.hlop.nl/challenge.php'
    let playerId = el.getAttribute('data-player')
    let guessInput = document.getElementsByName('player' + playerId)
    let guess = guessInput[0].value
    let guessInputParent = guessInput[0].parentElement
    guessInputParent.classList.remove('shake')
    if (guess >= 0 && guess <= 100 && !isNaN(guess)) {
        el.classList.add('pulse')
        fetch(baseUrl + "?player=" + playerId + "&guess=" + guess)
            .then((resp) => resp.json())
            .then(function(data) {
                let guess = data.guess
                switch (guess) {
                    case 'higher':
                        guessInputParent.style.background = 'green'
                        el.classList.remove('pulse')
                        break;
                    case 'lower':
                        guessInputParent.style.background = 'red'
                        el.classList.remove('pulse')
                        break;
                    case 'Bingo!!!':
                        let allBtns = document.querySelectorAll('.btn').forEach(function(btn) {
                            btn.disabled = true;
                        })
                        let victoryTime = document.createElement('p')
                        victoryTime.classList.add('pulse','victory')
                        victoryTime.innerHTML = 'Bingo!!!'
                        guessInputParent.appendChild(victoryTime);
                        guessInputParent.style.background = 'blue'
                        el.classList.remove('pulse')
                    default:
                }
            })
    } else {
        guessInputParent.classList.add('shake')
        guessInputParent.style.background = 'white'
    }
}